
const CHANNELS_PLAYLIST_DIR = 'channels-stream-playlist/';
const FFMPEG_FILE_REG = "file '";
const FLV_URL = 'http://localhost:8000/live/channel-';
const FLV_FILE_EXT = '.flv';
const SERVER_DELAY = 4;

module.exports = {
    CHANNELS_PLAYLIST_DIR,
    FFMPEG_FILE_REG,
    FLV_URL,
    FLV_FILE_EXT,
    SERVER_DELAY,
}